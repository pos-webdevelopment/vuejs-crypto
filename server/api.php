<?php
require_once('../server/controllers/RESTController.php');
// entry point for the rest api
// e.g. GET http://localhost/php41/api.php?r=credentials/25
// or with url_rewrite GET http://localhost/php41/api/credentials/25
// select route: credentials/25 -> controller=credentials, action=GET, id=25
$route = isset($_GET['r']) ? explode('/', trim($_GET['r'], '/')) : ['wallet'];
$controller = sizeof($route) > 0 ? $route[0] : 'wallet';

if ($controller == 'purchase') {
    require_once('../server/controllers/PurchaseRESTController.php');

    try {
        (new PurchaseRESTController())->handleRequest();
    } catch (Exception $e) {
        RESTController::responseHelper($e->getMessage(), $e->getCode());
    }
}
else if ($controller == 'wallet') {
    require_once('../server/controllers/WalletRESTController.php');

    try {
        (new WalletRESTController())->handleRequest();
    } catch(Exception $e) {
        RESTController::responseHelper($e->getMessage(), $e->getCode());
    }
}
else if ($controller == 'detail') {
    require_once('../server/controllers/DetailRESTController.php');

    try {
        (new DetailRESTController())->handleRequest();
    } catch(Exception $e) {
        RESTController::responseHelper($e->getMessage(), $e->getCode());
    }
}
else {
    RESTController::responseHelper('REST-Controller "' . $controller . '" not found', '404');
}
