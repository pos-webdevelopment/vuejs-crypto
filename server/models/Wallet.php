<?php

require_once "DatabaseObject.php";
require_once "Database.php";
class Wallet implements DatabaseObject
{
    public $wId = 0;
    public $wName = '';
    public $wCurrency = 0;

    /**
     * Wallet constructor.
     */
    public function __construct()
    {
    }

    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO tbl_wallet (wName, wCurrency) values(?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->wName, $this->wCurrency));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }

    public function update()
    {
        return null;
    }

    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM tbl_wallet where wId = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $item = $stmt->fetchObject('Wallet');
        Database::disconnect();
        return $item !== false ? $item : null;
    }

    public static function getAll()
    {
        $db = Database::connect();
        $sql = 'SELECT * FROM tbl_wallet ORDER BY wId';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $wallets = $stmt->fetchAll(PDO::FETCH_CLASS, 'Wallet');
        Database::disconnect();
        return $wallets;
    }

    public static function delete($id)
    {
        return null;
    }

    /**
     * @return int
     */
    public function getWId()
    {
        return $this->wId;
    }

    /**
     * @param int $wId
     * @return Wallet
     */
    public function setWId($wId)
    {
        $this->wId = $wId;
        return $this;
    }

    /**
     * @return string
     */
    public function getWName()
    {
        return $this->wName;
    }

    /**
     * @param string $wName
     * @return Wallet
     */
    public function setWName($wName)
    {
        $this->wName = $wName;
        return $this;
    }

    /**
     * @return int
     */
    public function getWCurrency()
    {
        return $this->wCurrency;
    }

    /**
     * @param int $wCurrency
     * @return Wallet
     */
    public function setWCurrency($wCurrency)
    {
        $this->wCurrency = $wCurrency;
        return $this;
    }
}