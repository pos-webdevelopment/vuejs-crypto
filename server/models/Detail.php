<?php

require_once "DatabaseObject.php";
require_once "Database.php";
require_once "Detail.php";
require_once "Wallet.php";
class Detail implements DatabaseObject // Purchase
{
    public $dId = 0;
    public $wId = 0;
    public $dAmount = 0.0;
    public $dPrice = 0.0;
    public $dDate = '';

    public $wName = '';
    public $wCurrency = '';

    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO tbl_detail (wId, dAmount, dPrice, dDate) values(?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $currentDate = date('Y-m-d H:i:s');
        $stmt->execute(array($this->wId, $this->dAmount, $this->dPrice, $currentDate));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }

    public function update()
    {
        return null;
    }

    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT *, sum(d.dAmount*d.dPrice) as sum FROM tbl_detail d LEFT JOIN tbl_wallet w ON d.wId = w.wId WHERE d.wId = ?;";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $item = $stmt->fetchObject('Detail');  // ORM
        Database::disconnect();
        return $item !== false ? $item : null;
    }

    public static function getAll()
    {
        $db = Database::connect();
        #$sql = 'SELECT *, d.dAmount*d.dPrice AS totalEuro FROM tbl_detail d RIGHT JOIN tbl_wallet w ON d.wId = w.wId;';
        $sql = 'SELECT *, sum(d.dAmount*d.dPrice) as sum FROM tbl_detail d LEFT JOIN tbl_wallet w ON d.wId = w.wId group by w.wId;';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Detail');
        Database::disconnect();
        return $items;
    }

    public static function getSum() {
        $db = Database::connect();
        $sql = "SELECT sum(dPrice*dAmount) AS sum FROM tbl_detail;";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $item = $stmt->fetchObject('Detail');  // ORM
        Database::disconnect();
        return $item !== false ? $item : null;
    }

    public static function getSingleDetail($id) {
        $db = Database::connect();
        $sql = 'SELECT * FROM tbl_detail d INNER JOIN tbl_wallet w ON d.wId = w.wId where d.wId = ?;';
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Detail');
        Database::disconnect();
        return $items;
    }


    public static function delete($id)
    {
        return null;
    }

    /**
     * @return int
     */
    public function getDId()
    {
        return $this->dId;
    }

    /**
     * @param int $dId
     * @return Detail
     */
    public function setDId($dId)
    {
        $this->dId = $dId;
        return $this;
    }

    /**
     * @return int
     */
    public function getWId()
    {
        return $this->wId;
    }

    /**
     * @param int $wId
     * @return Detail
     */
    public function setWId($wId)
    {
        $this->wId = $wId;
        return $this;
    }

    /**
     * @return float
     */
    public function getDAmount()
    {
        return $this->dAmount;
    }

    /**
     * @param float $dAmount
     * @return Detail
     */
    public function setDAmount($dAmount)
    {
        $this->dAmount = $dAmount;
        return $this;
    }

    /**
     * @return float
     */
    public function getDPrice()
    {
        return $this->dPrice;
    }

    /**
     * @param float $dPrice
     * @return Detail
     */
    public function setDPrice($dPrice)
    {
        $this->dPrice = $dPrice;
        return $this;
    }

    /**
     * @return string
     */
    public function getDDate()
    {
        return $this->dDate;
    }

    /**
     * @param string $dDate
     * @return Detail
     */
    public function setDDate($dDate)
    {
        $this->dDate = $dDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getWName()
    {
        return $this->wName;
    }

    /**
     * @param string $wName
     * @return Detail
     */
    public function setWName($wName)
    {
        $this->wName = $wName;
        return $this;
    }

    /**
     * @return string
     */
    public function getWCurrency()
    {
        return $this->wCurrency;
    }

    /**
     * @param string $wCurrency
     * @return Detail
     */
    public function setWCurrency($wCurrency)
    {
        $this->wCurrency = $wCurrency;
        return $this;
    }
}