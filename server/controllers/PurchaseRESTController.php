<?php

require_once('RESTController.php');
require_once('../server/models/Detail.php');

class PurchaseRESTController extends RESTController
{
    public function handleRequest()
    {
        switch ($this->method) {
            case 'GET':
                $this->handleGETRequest();
                break;
            case 'POST':
                $this->handlePOSTRequest();
                break;
            case 'PUT':
                $this->handlePUTRequest();
                break;
            case 'DELETE':
                $this->handleDELETERequest();
                break;
            default:
                $this->response('Method Not Allowed', 405);
                break;
        }
    }
    /**
     * get single/all
     */
    private function handleGETRequest()
    {
        if ($this->verb == null && sizeof($this->args) == 1) {
            $model = Detail::get($this->args[0]);  // single
            $this->response($model);
        } else if ($this->verb == null && empty($this->args)) {
            $model = Detail::getAll();             // all
            $this->response($model);
        } else {
            $this->response("Bad request", 400);
        }
    }

    /**
     * create
     */
    private function handlePOSTRequest()
    {
        $model = new Detail();
        $model->setWId($this->getDataOrNull('wId'));
        $model->setDAmount($this->getDataOrNull('dAmount'));
        $model->setDPrice($this->getDataOrNull('dPrice'));
        $model->setDDate($this->getDataOrNull('dDate'));

        if ($model->create()) {
            $this->response("OK", 201);
        } else {
            $this->response("Bad request", 400);
        }
    }

    /**
     * update
     */
    private function handlePUTRequest()
    {}

    /**
     * delete
     */
    private function handleDELETERequest()
    {}
}