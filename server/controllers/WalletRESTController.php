<?php

require_once('RESTController.php');
require_once('../server/models/Wallet.php');

class WalletRESTController extends RESTController
{
    public function handleRequest()
    {
        switch ($this->method) {
            case 'GET':
                $this->handleGETRequest();
                break;
            case 'POST':
                $this->handlePOSTRequest();
                break;
            case 'PUT':
                $this->handlePUTRequest();
                break;
            case 'DELETE':
                $this->handleDELETERequest();
                break;
            default:
                $this->response('Method Not Allowed', 405);
                break;
        }
    }
    /**
     * get all/single wallet
     */
    private function handleGETRequest()
    {
        if ($this->verb == null && sizeof($this->args) == 0) {
            $model = Wallet::getAll();
            $this->response($model);
        } else if ($this->verb == null && sizeof($this->args) == 1) {
            $model = Wallet::get($this->args[0]);
            if ($model == null) {
                $this->response("Not found", 404);
            } else {
                $this->response($model);
            }
        } else {
            $this->response("Bad request", 400);
        }
    }

    /**
     * create
     */
    private function handlePOSTRequest()
    {
        $model = new Wallet();
        
        $model->setWName($this->getDataOrNull('wName'));
        $model->setWCurrency($this->getDataOrNull('wCurrency'));
    
        if ($model->create()) {
            $this->response("OK", 200);
        } else {
            $this->response("Bad Request", 400);
        }
    }

    /**
     * update
     */
    private function handlePUTRequest()
    {}

    /**
     * delete
     */
    private function handleDELETERequest()
    {}
}