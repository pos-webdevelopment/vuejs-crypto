<?php

require_once "RESTController.php";
require_once "../server/models/Detail.php";
class DetailRESTController extends RESTController
{

    public function handleRequest()
    {
        switch ($this->method) {
            case 'GET':
                $this->handleGETRequest();
                break;
            case 'POST':
                $this->handlePOSTRequest();
                break;
            case 'PUT':
                $this->handlePUTRequest();
                break;
            case 'DELETE':
                $this->handleDELETERequest();
                break;
            default:
                $this->response('Method Not Allowed', 405);
                break;
        }
    }
    /**
     * get single/all
     */
    private function handleGETRequest()
    {
        if ($this->verb == null && sizeof($this->args) == 1) {
            $model = Detail::getSingleDetail($this->args[0]);  // single
            $this->response($model);
        } else if ($this->verb == null && empty($this->args)) {
            $model = Detail::getSum();             // all
            $this->response($model);
        } else {
            $this->response("Bad request", 400);
        }
    }

    /**
     * create
     */
    private function handlePOSTRequest()
    {}

    /**
     * update
     */
    private function handlePUTRequest()
    {}

    /**
     * delete
     */
    private function handleDELETERequest()
    {}
}